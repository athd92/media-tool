MEDIAS = {
    "le_parisien": "https://feeds.leparisien.fr/leparisien/rss",
    "le_monde": "https://www.lemonde.fr/rss/en_continu.xml",
    "le_figaro": "https://www.lefigaro.fr/rss/figaro_actualites.xml",
    "france_info": "https://www.francetvinfo.fr/titres.rss",
    "bfm": "https://www.bfmtv.com/rss/news-24-7/",
}
