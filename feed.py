import requests
from displayer import Displayer


class Feed:
    def __init__(self, media, url=""):
        self.media = media
        self.url = url
        self.raw_data = self.get_raw_data()

    def __str__(self):
        return f"{self.media}"

    def get_raw_data(self):
        response = requests.get(self.url)
        return response.content if response.status_code == 200 else "request failed"

    def get_pretty_raw_data(self):
        if self.raw_data != "request failed":
            displayer = Displayer(self.media, self.raw_data)
            displayer.display_table()

    def get_pretty_filtered_data(self, word):
        if self.raw_data != "request failed":
            displayer = Displayer(self.media, self.raw_data)
            displayer.display_filtered_table(word)