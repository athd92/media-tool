from rich.console import Console
import feedparser
from rich.table import Table
from utils import cprint


class Displayer:
    """
    Displayer class is used to displayed infos
    """
    def __init__(self, media, infos):
        self.media = media
        self.infos = infos
        self.console = Console()
        self.table = Table(title=f"[bold green]**{self.media}**[/]")

    def display_table(self):
        # Display a table with fetched rss infos
        cprint('')
        feed = feedparser.parse(self.infos)
        self.table.add_column(
            "Media", justify="left", style="cyan", no_wrap=True,
        )
        self.table.add_column("Titre", style="bright_yellow")
        self.table.add_column("Url", justify="left", style="green")
        for article in feed.entries:
            self.table.add_row(
                self.media, f"{article.get('title')}", f"{article.get('link')}",
            )
        self.console.print(self.table)

    def display_filtered_table(self, word):
        # Display a table with fetched rss infos

        feed = feedparser.parse(self.infos)
        self.table.add_column(
            "Media", justify="left", style="cyan", no_wrap=True,
        )
        self.table.add_column("Titre", style="bright_yellow")
        self.table.add_column("Url", justify="left", style="green")
        display = False
        for article in feed.entries:
            if word.lower() in article.get("title").split():
                self.table.add_row(
                    self.media, f"{article.get('title')}", f"{article.get('link')}",
                )
                display = True
        if display:
            self.console.print(self.table)
        else:
            cprint(f"Aucun article trouvé avec le mot {word}")
