from rich.console import Console
from media_selection import MEDIAS
from feed import Feed
from utils import cprint

console = Console()

all_medias = {}
count = 0
for k, v in MEDIAS.items():
    all_medias[count] = Feed(k, url=v)
    count += 1

cprint('')
console.print("[1] Recherche par mot ?")
console.print("[2] Recherche par media ?")
console.print("[3] Tous les medias ?")
console.print("[4] Exit\n")
choice = console.input("[*] > ")
cprint('')

if choice == str(1):
    word = console.input("Quel mot rechercher? ")

    for k, v in all_medias.items():
        console.print(f"[{k}] {v}")
    choice = console.input("\nDans quel média? ")

    if int(choice) not in list(all_medias.keys()):
        cprint("Erreur d'entrée, média invalid")
        exit(0)
    feed = all_medias[int(choice)]
    feed.get_pretty_filtered_data(word)

elif choice == str(2):
    for k, v in all_medias.items():
        console.print(f"[{k}] {v}")

    choice = console.input("\nQuel média? ")
    if int(choice) not in list(all_medias.keys()):
        cprint("Erreur d'entrée, média invalid")
        exit(0)

    feed = all_medias[int(choice)]
    feed.get_pretty_raw_data()

elif choice == str(3):
    for feed in all_medias.values():
        feed.get_pretty_raw_data()

else:
    exit(0)