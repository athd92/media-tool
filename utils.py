from rich.console import Console
import os

console = Console()


def cprint(message):
    os.system('cls' if os.name == 'nt' else 'clear')
    console.print(message)


def display_medias_choice(medias):
    for i in (medias.keys()):
        if i is not None:
            print(f"[] {i}\n")
